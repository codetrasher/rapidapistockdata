[![Build status](https://ci.appveyor.com/api/projects/status/p1ux24m8b38r6t7c/branch/master?svg=true)](https://ci.appveyor.com/project/codetrasher/rapidapistockdata/branch/master)

# RapidAPIStockData

A .NET RapidAPI API client for Yahoo Stock data.

## Installation

From command line simply typing

```PowerShell
PM> Install-Package RapidAPIStockData
```

should install the package just fine.

## How to use

This client is using [Stock Data - Yahoo Finance alternative](https://rapidapi.com/principalapis/api/stock-data-yahoo-finance-alternative/) to fetching stock information.

In order to use this client in the first place, one needs to register for an API key. There is a free subscription plan, as well.

### Setting up the client

The client uses `IOptions<T>` interface internally for accessing data like API keys. To create `IOptions` for the client, one can do something like this:

```csharp
IOptions<ClientOptions> options = Options.Create(new ClientOptions
{
    ApiKey = "<your_api_key_here>"
});
```

Then, one can simply inject `Options` to the target service, for example:

```csharp
new StockService(options);
```

After creating the service one can then call endpoints supported by each service. In the case of `IMarketService`, to use the autocomplete search endpoint, one would type

```csharp
List<StockSearchResult> searchResult = await marketService.GetStockAutoComplete("appl", Language.EN);
```

## Supported endpoints

Unfortunately, not all of the endpoints of the API have been implemented as I implemented those with higher importance to me. I'm planning to add the rest as soon as possible.

Here is the progress so far.

| Stock endpoint | Is implemented |
|:----------|:----------------:|
|GET Quotes| - |
|GET Option chain| - |
|GET Stock history| - |
|GET Stock details| x |
|GET Chart| - |

| Market endpoint | Is implemented |
|:----------|:----------------:|
|GET Stock autocomplete| x |
|GET Trending stocks| - |
|GET Market Insights| - |
|GET Popular stocks| - |
|GET Similar stocks| - |
|GET Market summary| - |

## Bugs or other issues
Please, if you notice a bug, a typo or want me to hurry up with missing endpoints, don't hesitate to open an issue.

## Donate

If you like what I do, please, consider leaving a little coffee money. Thanks!

LTC: LdUnd9sMNpshuck1Up3Yrk9Kni5CA8iif2

XMR: 41iPpcpkAnaeUdqm1NfUaTZuyWsWK4Sc2FmBvqHeVsQLh3eDnLDiUtM9iCUSMsYegqTCoFuRdmrg2eiFkV5kmgk88LWdWD9

XNO: nano_3xyd4ietsuxcjax3u67x9ns5eztnuqoe186ixjti1z3311szdtwdk7q1e7xy

ADA: addr1qxc43hkcpf2fgvdkmcdp647wnyt3tz48zk09ztufkjm83pd3tr0dszj5jscmdhs6r4tuaxghzk92w9v72yhcnd9k0zzs0shsq2