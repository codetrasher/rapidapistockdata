﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace RapidAPIStockData.Extensions
{
    internal static class HttpClientExtensions
    {
        public static async Task<T> GetJsonWithHeadersAsync<T>(this HttpClient client, string uri, Dictionary<string, string> headers)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            foreach (var header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            using var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var responseStream = await response.Content.ReadAsStreamAsync();
            return await JsonSerializer.DeserializeAsync<T>(responseStream);
        }
    }
}
