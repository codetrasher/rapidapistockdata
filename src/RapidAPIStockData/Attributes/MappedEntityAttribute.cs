﻿using System;

namespace RapidAPIStockData.Attributes
{
    internal class MappedEntityAttribute : Attribute
    {
        public Type MappedType { get; set; }

        public MappedEntityAttribute(Type mappedType)
        {
            MappedType = mappedType;
        }
    }
}