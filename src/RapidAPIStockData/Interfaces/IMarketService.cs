﻿using RapidAPIStockData.Entities.Enums;
using RapidAPIStockData.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RapidAPIStockData.Services.AutoCompleteService
{
    public interface IMarketService
    {
        Task<IEnumerable<StockSearchResult>> GetStockAutoComplete(string searchQuery, Language? language = Language.EN, Region? region = null);
    }
}
