﻿using RapidAPIStockData.Entities.Enums;
using RapidAPIStockData.Entities.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RapidAPIStockData.Interfaces
{
    public interface IStockService
    {
        Task<QuoteSummaryResult> GetStockDetailsAsync(string symbol, IEnumerable<QuoteSummaryModule> modules, Language? language = null, Region? region = null);
    }
}
