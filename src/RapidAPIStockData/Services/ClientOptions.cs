﻿namespace RapidAPIStockData.Services
{
    public class ClientOptions
    {
        public string ApiKey { get; set; }

        internal string HostUrl { get; set; } = "stock-data-yahoo-finance-alternative.p.rapidapi.com";
    }
}
