﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace RapidAPIStockData.Services
{
    internal static class Headers
    {
        public static Dictionary<string, string> Create(IOptions<ClientOptions> options)
        {
            return new Dictionary<string, string>
            {
                ["x-rapidapi-host"] = options.Value.HostUrl,
                ["x-rapidapi-key"] = options.Value.ApiKey
            };
        }
    }
}
