﻿using Microsoft.Extensions.Options;
using RapidAPIStockData.Entities.Enums;
using RapidAPIStockData.Entities.Responses;
using RapidAPIStockData.Extensions;
using RapidAPIStockData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace RapidAPIStockData.Services
{
    public class StockService : IStockService
    {
        private const string STOCK_DETAILS_ENDPOINT = "/v11/finance/quoteSummary";
        private IOptions<ClientOptions> _options;
        private HttpClient _client;

        public StockService(IOptions<ClientOptions> options, HttpClient client)
        {
            _options = options;
            _client = client;
        }

        public StockService(IOptions<ClientOptions> options) : this(options, new HttpClient()) { }

        public Task<QuoteSummaryResult> GetStockDetailsAsync(string symbol, IEnumerable<QuoteSummaryModule> modules, Language? language = null, Region? region = null)
        {
            if (symbol.Trim().Length == 0)
            {
                throw new ArgumentException("symbol can't be empty");
            }

            return GetStockDetailsInternalAsync(symbol, modules, language, region);
        }

        private async Task<QuoteSummaryResult> GetStockDetailsInternalAsync(string symbol, IEnumerable<QuoteSummaryModule> modules, Language? language = null, Region? region = null)
        {
            var builder = new UriBuilder(new Uri(new Uri("https://" + _options.Value.HostUrl), $"{STOCK_DETAILS_ENDPOINT}/{symbol}"));
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["modules"] = modules.ToCommaSeparatedString();
            query["lang"] = language != null ? ((Language)language).String() : null;
            query["region"] = region != null ? ((Region)region).String() : null;
            builder.Query = query.ToString();

            var headers = Headers.Create(_options);

            var quoteSummaryResult = await _client.GetJsonWithHeadersAsync<QuoteSummaryResult>(builder.Uri.ToString(), headers);

            var resultArray = quoteSummaryResult.QuoteSummary.Result as IEnumerable<object>;
            var result = (JsonElement)resultArray.FirstOrDefault();

            var moduleCollection = QuoteSummaryModuleHelper.GetDictionary();

            var elementList = result.EnumerateObject();
            foreach (var element in elementList)
            {
                var mod = moduleCollection[element.Name];
                //var instance = mod.CreateObjectFromMappedType();
                Type type = mod.GetMappedType();
                quoteSummaryResult.QuoteSummary.ParseResult[element.Name] = JsonSerializer.Deserialize(element.Value.GetRawText(), type);
            }


            return quoteSummaryResult;
        }
    }
}
