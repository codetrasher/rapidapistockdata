﻿using Microsoft.Extensions.Options;
using RapidAPIStockData.Entities.Enums;
using RapidAPIStockData.Entities.Responses;
using RapidAPIStockData.Extensions;
using RapidAPIStockData.Services.AutoCompleteService;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace RapidAPIStockData.Services
{
    public class MarketService : IMarketService
    {
        private const string AUTOCOMPLETE_ENDPOINT = "v6/finance/autocomplete";

        private readonly IOptions<ClientOptions> _options;
        private readonly HttpClient _httpClient;

        public MarketService(IOptions<ClientOptions> options)
            : this(options, new HttpClient())
        { }

        public MarketService(IOptions<ClientOptions> options, HttpClient client)
        {
            _options = options;
            _httpClient = client;
        }

        public async Task<IEnumerable<StockSearchResult>> GetStockAutoComplete(string searchQuery, Language? language = Language.EN, Region? region = null)
        {
            var builder = new UriBuilder(new Uri(new Uri("https://" + _options.Value.HostUrl), AUTOCOMPLETE_ENDPOINT))
            {
                Query = $"query={searchQuery}"
            };
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["lang"] = ((Language)language).String();

            if (region != null)
                query["region"] = ((Region)region).String();
            builder.Query = query.ToString();

            var headers = Headers.Create(_options);

            var result = await _httpClient.GetJsonWithHeadersAsync<AutoCompleteSearchResult>(builder.Uri.ToString(), headers);
            return result.ResultSet.Result;
        }
    }
}
