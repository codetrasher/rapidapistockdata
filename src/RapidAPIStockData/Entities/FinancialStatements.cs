﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class IncomeStatementHistory
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("endDate")]
        public EndDate EndDate { get; set; }

        [JsonPropertyName("totalRevenue")]
        public TotalRevenue TotalRevenue { get; set; }

        [JsonPropertyName("costOfRevenue")]
        public CostOfRevenue CostOfRevenue { get; set; }

        [JsonPropertyName("grossProfit")]
        public GrossProfit GrossProfit { get; set; }

        [JsonPropertyName("researchDevelopment")]
        public ResearchDevelopment ResearchDevelopment { get; set; }

        [JsonPropertyName("sellingGeneralAdministrative")]
        public SellingGeneralAdministrative SellingGeneralAdministrative { get; set; }

        [JsonPropertyName("nonRecurring")]
        public NonRecurring NonRecurring { get; set; }

        [JsonPropertyName("otherOperatingExpenses")]
        public OtherOperatingExpenses OtherOperatingExpenses { get; set; }

        [JsonPropertyName("totalOperatingExpenses")]
        public TotalOperatingExpenses TotalOperatingExpenses { get; set; }

        [JsonPropertyName("operatingIncome")]
        public OperatingIncome OperatingIncome { get; set; }

        [JsonPropertyName("totalOtherIncomeExpenseNet")]
        public TotalOtherIncomeExpenseNet TotalOtherIncomeExpenseNet { get; set; }

        [JsonPropertyName("ebit")]
        public Ebit Ebit { get; set; }

        [JsonPropertyName("interestExpense")]
        public InterestExpense InterestExpense { get; set; }

        [JsonPropertyName("incomeBeforeTax")]
        public IncomeBeforeTax IncomeBeforeTax { get; set; }

        [JsonPropertyName("incomeTaxExpense")]
        public IncomeTaxExpense IncomeTaxExpense { get; set; }

        [JsonPropertyName("minorityInterest")]
        public MinorityInterest MinorityInterest { get; set; }

        [JsonPropertyName("netIncomeFromContinuingOps")]
        public NetIncomeFromContinuingOps NetIncomeFromContinuingOps { get; set; }

        [JsonPropertyName("discontinuedOperations")]
        public DiscontinuedOperations DiscontinuedOperations { get; set; }

        [JsonPropertyName("extraordinaryItems")]
        public ExtraordinaryItems ExtraordinaryItems { get; set; }

        [JsonPropertyName("effectOfAccountingCharges")]
        public EffectOfAccountingCharges EffectOfAccountingCharges { get; set; }

        [JsonPropertyName("otherItems")]
        public OtherItems OtherItems { get; set; }

        [JsonPropertyName("netIncome")]
        public NetIncome NetIncome { get; set; }

        [JsonPropertyName("netIncomeApplicableToCommonShares")]
        public NetIncomeApplicableToCommonShares NetIncomeApplicableToCommonShares { get; set; }

        [JsonPropertyName("incomeStatementHistory")]
        public List<IncomeStatementHistory> IncomeStatementHistoryItems { get; set; }
    }

    public class IncomeStatementHistoryQuarterly
    {
        [JsonPropertyName("incomeStatementHistory")]
        public List<IncomeStatementHistory> IncomeStatementHistory { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }
    public class CashflowStatement
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("endDate")]
        public EndDate EndDate { get; set; }

        [JsonPropertyName("netIncome")]
        public NetIncome NetIncome { get; set; }

        [JsonPropertyName("depreciation")]
        public Depreciation Depreciation { get; set; }

        [JsonPropertyName("changeToNetincome")]
        public ChangeToNetincome ChangeToNetincome { get; set; }

        [JsonPropertyName("changeToAccountReceivables")]
        public ChangeToAccountReceivables ChangeToAccountReceivables { get; set; }

        [JsonPropertyName("changeToLiabilities")]
        public ChangeToLiabilities ChangeToLiabilities { get; set; }

        [JsonPropertyName("changeToInventory")]
        public ChangeToInventory ChangeToInventory { get; set; }

        [JsonPropertyName("changeToOperatingActivities")]
        public ChangeToOperatingActivities ChangeToOperatingActivities { get; set; }

        [JsonPropertyName("totalCashFromOperatingActivities")]
        public TotalCashFromOperatingActivities TotalCashFromOperatingActivities { get; set; }

        [JsonPropertyName("capitalExpenditures")]
        public CapitalExpenditures CapitalExpenditures { get; set; }

        [JsonPropertyName("investments")]
        public Investments Investments { get; set; }

        [JsonPropertyName("otherCashflowsFromInvestingActivities")]
        public OtherCashflowsFromInvestingActivities OtherCashflowsFromInvestingActivities { get; set; }

        [JsonPropertyName("totalCashflowsFromInvestingActivities")]
        public TotalCashflowsFromInvestingActivities TotalCashflowsFromInvestingActivities { get; set; }

        [JsonPropertyName("dividendsPaid")]
        public DividendsPaid DividendsPaid { get; set; }

        [JsonPropertyName("netBorrowings")]
        public NetBorrowings NetBorrowings { get; set; }

        [JsonPropertyName("otherCashflowsFromFinancingActivities")]
        public OtherCashflowsFromFinancingActivities OtherCashflowsFromFinancingActivities { get; set; }

        [JsonPropertyName("totalCashFromFinancingActivities")]
        public TotalCashFromFinancingActivities TotalCashFromFinancingActivities { get; set; }

        [JsonPropertyName("changeInCash")]
        public ChangeInCash ChangeInCash { get; set; }

        [JsonPropertyName("repurchaseOfStock")]
        public RepurchaseOfStock RepurchaseOfStock { get; set; }

        [JsonPropertyName("issuanceOfStock")]
        public IssuanceOfStock IssuanceOfStock { get; set; }
    }

    public class CashflowStatementHistory
    {
        [JsonPropertyName("cashflowStatements")]
        public List<CashflowStatement> CashflowStatements { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class BalanceSheetHistory
    {
        [JsonPropertyName("balanceSheetStatements")]
        public List<BalanceSheetStatement> BalanceSheetStatements { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class BalanceSheetHistoryQuarterly
    {
        [JsonPropertyName("balanceSheetStatements")]
        public List<BalanceSheetStatement> BalanceSheetStatements { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class BalanceSheetStatement
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("endDate")]
        public EndDate EndDate { get; set; }

        [JsonPropertyName("cash")]
        public Cash Cash { get; set; }

        [JsonPropertyName("shortTermInvestments")]
        public ShortTermInvestments ShortTermInvestments { get; set; }

        [JsonPropertyName("netReceivables")]
        public NetReceivables NetReceivables { get; set; }

        [JsonPropertyName("inventory")]
        public Inventory Inventory { get; set; }

        [JsonPropertyName("otherCurrentAssets")]
        public OtherCurrentAssets OtherCurrentAssets { get; set; }

        [JsonPropertyName("totalCurrentAssets")]
        public TotalCurrentAssets TotalCurrentAssets { get; set; }

        [JsonPropertyName("longTermInvestments")]
        public LongTermInvestments LongTermInvestments { get; set; }

        [JsonPropertyName("propertyPlantEquipment")]
        public PropertyPlantEquipment PropertyPlantEquipment { get; set; }

        [JsonPropertyName("otherAssets")]
        public OtherAssets OtherAssets { get; set; }

        [JsonPropertyName("totalAssets")]
        public TotalAssets TotalAssets { get; set; }

        [JsonPropertyName("accountsPayable")]
        public AccountsPayable AccountsPayable { get; set; }

        [JsonPropertyName("shortLongTermDebt")]
        public ShortLongTermDebt ShortLongTermDebt { get; set; }

        [JsonPropertyName("otherCurrentLiab")]
        public OtherCurrentLiab OtherCurrentLiab { get; set; }

        [JsonPropertyName("longTermDebt")]
        public LongTermDebt LongTermDebt { get; set; }

        [JsonPropertyName("otherLiab")]
        public OtherLiab OtherLiab { get; set; }

        [JsonPropertyName("totalCurrentLiabilities")]
        public TotalCurrentLiabilities TotalCurrentLiabilities { get; set; }

        [JsonPropertyName("totalLiab")]
        public TotalLiab TotalLiab { get; set; }

        [JsonPropertyName("commonStock")]
        public CommonStock CommonStock { get; set; }

        [JsonPropertyName("retainedEarnings")]
        public RetainedEarnings RetainedEarnings { get; set; }

        [JsonPropertyName("treasuryStock")]
        public TreasuryStock TreasuryStock { get; set; }

        [JsonPropertyName("otherStockholderEquity")]
        public OtherStockholderEquity OtherStockholderEquity { get; set; }

        [JsonPropertyName("totalStockholderEquity")]
        public TotalStockholderEquity TotalStockholderEquity { get; set; }

        [JsonPropertyName("netTangibleAssets")]
        public NetTangibleAssets NetTangibleAssets { get; set; }
    }

    public class EndDate : IntJsonEntity
    {
    }

    public class TotalRevenue : LongJsonEntity
    {
    }

    public class CostOfRevenue : LongJsonEntity
    {
    }

    public class GrossProfit : LongJsonEntity
    {
    }

    public class ResearchDevelopment : LongJsonEntity
    {
    }

    public class SellingGeneralAdministrative : LongJsonEntity
    {
    }

    public class NonRecurring
    {
    }

    public class OtherOperatingExpenses
    {
    }

    public class TotalOperatingExpenses : LongJsonEntity
    {
    }

    public class OperatingIncome : LongJsonEntity
    {
    }

    public class TotalOtherIncomeExpenseNet : LongJsonEntity
    {
    }

    public class Ebit : LongJsonEntity
    {
    }

    public class InterestExpense : LongJsonEntity
    {
    }

    public class IncomeBeforeTax : LongJsonEntity
    {
    }

    public class IncomeTaxExpense : LongJsonEntity
    {
    }

    public class MinorityInterest
    {
    }

    public class NetIncomeFromContinuingOps : LongJsonEntity
    {
    }

    public class DiscontinuedOperations
    {
    }

    public class ExtraordinaryItems
    {
    }

    public class EffectOfAccountingCharges
    {
    }

    public class OtherItems
    {
    }

    public class NetIncomeApplicableToCommonShares : LongJsonEntity
    {
    }

    public class NetIncome : LongJsonEntity
    {
    }

    public class Depreciation : LongJsonEntity
    {
    }

    public class ChangeToNetincome : LongJsonEntity
    {
    }

    public class ChangeToAccountReceivables : LongJsonEntity
    {
    }

    public class ChangeToLiabilities : LongJsonEntity
    {
    }

    public class ChangeToInventory : LongJsonEntity
    {
    }

    public class ChangeToOperatingActivities : LongJsonEntity
    {
    }

    public class TotalCashFromOperatingActivities : LongJsonEntity
    {
    }

    public class CapitalExpenditures : LongJsonEntity
    {
    }

    public class Investments : LongJsonEntity
    {
    }

    public class OtherCashflowsFromInvestingActivities : LongJsonEntity
    {
    }

    public class TotalCashflowsFromInvestingActivities : LongJsonEntity
    {
    }

    public class DividendsPaid : LongJsonEntity
    {
    }

    public class NetBorrowings : LongJsonEntity
    {
    }

    public class OtherCashflowsFromFinancingActivities : LongJsonEntity
    {
    }

    public class TotalCashFromFinancingActivities : LongJsonEntity
    {
    }

    public class ChangeInCash : LongJsonEntity
    {
    }

    public class RepurchaseOfStock : LongJsonEntity
    {
    }

    public class IssuanceOfStock : IntJsonEntity
    {
    }

    public class Cash : LongJsonEntity
    {
    }

    public class ShortTermInvestments : LongJsonEntity
    {
    }

    public class NetReceivables : LongJsonEntity
    {
    }

    public class Inventory : LongJsonEntity
    {
    }

    public class OtherCurrentAssets : LongJsonEntity
    {
    }

    public class TotalCurrentAssets : LongJsonEntity
    {
    }

    public class LongTermInvestments : LongJsonEntity
    {
    }

    public class PropertyPlantEquipment : LongJsonEntity
    {
    }

    public class OtherAssets : LongJsonEntity
    {
    }

    public class AccountsPayable : LongJsonEntity
    {
    }

    public class ShortLongTermDebt : LongJsonEntity
    {
    }

    public class OtherCurrentLiab : LongJsonEntity
    {
    }

    public class LongTermDebt : LongJsonEntity
    {
    }

    public class OtherLiab : LongJsonEntity
    {
    }

    public class TotalCurrentLiabilities : LongJsonEntity
    {
    }

    public class TotalLiab : LongJsonEntity
    {
    }

    public class CommonStock : LongJsonEntity
    {
    }

    public class TreasuryStock : LongJsonEntity
    {
    }

    public class OtherStockholderEquity : LongJsonEntity
    {
    }

    public class TotalStockholderEquity : LongJsonEntity
    {
    }

    public class NetTangibleAssets : LongJsonEntity
    {
    }

    public class CashflowStatementHistoryQuarterly
    {
        [JsonPropertyName("cashflowStatements")]
        public List<CashflowStatement> CashflowStatements { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }
}
