﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class SummaryDetail
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
        [JsonPropertyName("priceHint")]
        public PriceHint PriceHint { get; set; }
        [JsonPropertyName("previousClose")]
        public PreviousClose PreviousClose { get; set; }
        [JsonPropertyName("open")]
        public Open Open { get; set; }
        [JsonPropertyName("dayLow")]
        public DayLow DayLow { get; set; }
        [JsonPropertyName("dayHigh")]
        public DayHigh DayHigh { get; set; }
        [JsonPropertyName("regularMarketPreviousClose")]
        public RegularMarketPreviousClose RegularMarketPreviousClose { get; set; }
        [JsonPropertyName("regularMarketOpen")]
        public RegularMarketOpen RegularMarketOpen { get; set; }
        [JsonPropertyName("regularMarketDayLow")]
        public RegularMarketDayLow RegularMarketDayLow { get; set; }
        [JsonPropertyName("regularMarketDayHigh")]
        public RegularMarketDayHigh RegularMarketDayHigh { get; set; }
        [JsonPropertyName("dividendRate")]
        public DividendRate DividendRate { get; set; }
        [JsonPropertyName("dividendYield")]
        public DividendYield DividendYield { get; set; }
        [JsonPropertyName("exDividendDate")]
        public ExDividendDate ExDividendDate { get; set; }
        [JsonPropertyName("payoutRatio")]
        public PayoutRatio PayoutRatio { get; set; }
        [JsonPropertyName("fiveYearAvgDividendYield")]
        public FiveYearAvgDividendYield FiveYearAvgDividendYield { get; set; }
        [JsonPropertyName("beta")]
        public Beta Beta { get; set; }
        [JsonPropertyName("trailingPE")]
        public TrailingPE TrailingPE { get; set; }
        [JsonPropertyName("forwardPE")]
        public ForwardPE ForwardPE { get; set; }
        [JsonPropertyName("volume")]
        public Volume Volume { get; set; }
        [JsonPropertyName("regularMarketVolume")]
        public RegularMarketVolume RegularMarketVolume { get; set; }
        [JsonPropertyName("averageVolume")]
        public AverageVolume AverageVolume { get; set; }
        [JsonPropertyName("averageVolume10days")]
        public AverageVolume10Days AverageVolume10days { get; set; }
        [JsonPropertyName("averageDailyVolume10Day")]
        public AverageDailyVolume10Day AverageDailyVolume10Day { get; set; }
        [JsonPropertyName("bid")]
        public Bid Bid { get; set; }
        [JsonPropertyName("ask")]
        public Ask Ask { get; set; }
        [JsonPropertyName("bidSize")]
        public BidSize BidSize { get; set; }
        [JsonPropertyName("askSize")]
        public AskSize AskSize { get; set; }
        [JsonPropertyName("marketCap")]
        public MarketCap MarketCap { get; set; }
        [JsonPropertyName("yield")]
        public Yield Yield { get; set; }
        [JsonPropertyName("ytdReturn")]
        public YtdReturn YtdReturn { get; set; }
        [JsonPropertyName("totalAssets")]
        public TotalAssets TotalAssets { get; set; }
        [JsonPropertyName("expireDate")]
        public ExpireDate ExpireDate { get; set; }
        [JsonPropertyName("strikePrice")]
        public StrikePrice StrikePrice { get; set; }
        [JsonPropertyName("openInterest")]
        public OpenInterest OpenInterest { get; set; }
        [JsonPropertyName("fiftyTwoWeekLow")]
        public FiftyTwoWeekLow FiftyTwoWeekLow { get; set; }
        [JsonPropertyName("fiftyTwoWeekHigh")]
        public FiftyTwoWeekHigh FiftyTwoWeekHigh { get; set; }
        [JsonPropertyName("priceToSalesTrailing12Months")]
        public PriceToSalesTrailing12Months PriceToSalesTrailing12Months { get; set; }
        [JsonPropertyName("fiftyDayAverage")]
        public FiftyDayAverage FiftyDayAverage { get; set; }
        [JsonPropertyName("twoHundredDayAverage")]
        public TwoHundredDayAverage TwoHundredDayAverage { get; set; }
        [JsonPropertyName("trailingAnnualDividendRate")]
        public TrailingAnnualDividendRate TrailingAnnualDividendRate { get; set; }
        [JsonPropertyName("trailingAnnualDividendYield")]
        public TrailingAnnualDividendYield TrailingAnnualDividendYield { get; set; }
        [JsonPropertyName("navPrice")]
        public Navprice NavPrice { get; set; }
        [JsonPropertyName("currency")]
        public string Currency { get; set; }
        [JsonPropertyName("fromCurrency")]
        public object FromCurrency { get; set; }
        [JsonPropertyName("toCurrency")]
        public object ToCurrency { get; set; }
        [JsonPropertyName("lastMarket")]
        public object LastMarket { get; set; }
        [JsonPropertyName("volume24Hr")]
        public Volume24Hr Volume24Hr { get; set; }
        [JsonPropertyName("volumeAllCurrencies")]
        public VolumeAllCurrencies VolumeAllCurrencies { get; set; }
        [JsonPropertyName("circulatingSupply")]
        public CirculatingSupply CirculatingSupply { get; set; }
        [JsonPropertyName("algorithm")]
        public object Algorithm { get; set; }
        [JsonPropertyName("maxSupply")]
        public MaxSupply MaxSupply { get; set; }
        [JsonPropertyName("startDate")]
        public StartDate StartDate { get; set; }
        [JsonPropertyName("tradeable")]
        public bool Tradeable { get; set; }
    }

    public class PriceHint : IntJsonEntity
    {
    }

    public class PreviousClose : FloatJsonEntity
    {
    }

    public class Open : FloatJsonEntity
    {
    }

    public class DayLow : FloatJsonEntity
    {
    }

    public class DayHigh : FloatJsonEntity
    {
    }

    public class RegularMarketPreviousClose : DoubleJsonEntity
    {
    }

    public class RegularMarketOpen : DoubleJsonEntity
    {
    }

    public class RegularMarketDayLow : DoubleJsonEntity
    {
    }

    public class RegularMarketDayHigh : DoubleJsonEntity
    {
    }

    public class DividendRate : FloatJsonEntity
    {
    }

    public class DividendYield : FloatJsonEntity
    {
    }

    public class ExDividendDate : IntJsonEntity
    {
    }

    public class PayoutRatio : FloatJsonEntity
    {
    }

    public class FiveYearAvgDividendYield : FloatJsonEntity
    {
    }

    public class Beta : FloatJsonEntity
    {
    }

    public class TrailingPE : FloatJsonEntity
    {
    }

    public class ForwardPE : FloatJsonEntity
    {
    }

    public class Volume : IntJsonEntity
    {
    }

    public class RegularMarketVolume : IntJsonEntity
    {
    }

    public class AverageVolume : IntJsonEntity
    {
    }

    public class AverageVolume10Days : IntJsonEntity
    {
    }

    public class AverageDailyVolume10Day : IntJsonEntity
    {
    }

    public class Bid : FloatJsonEntity
    {
    }

    public class Ask : FloatJsonEntity
    {
    }

    public class BidSize : IntJsonEntity
    {
    }

    public class AskSize : IntJsonEntity
    {
    }

    public class MarketCap : LongJsonEntity
    {
    }

    public class Yield
    {
    }

    public class YtdReturn
    {
    }

    public class ExpireDate
    {
    }

    public class StrikePrice
    {
    }

    public class OpenInterest
    {
    }

    public class FiftyTwoWeekLow : FloatJsonEntity
    {
    }

    public class FiftyTwoWeekHigh : FloatJsonEntity
    {
    }

    public class PriceToSalesTrailing12Months : FloatJsonEntity
    {
    }

    public class FiftyDayAverage : FloatJsonEntity
    {
    }

    public class TwoHundredDayAverage : FloatJsonEntity
    {
    }

    public class TrailingAnnualDividendRate : FloatJsonEntity
    {
    }

    public class TrailingAnnualDividendYield : FloatJsonEntity
    {
    }

    public class Navprice
    {
    }

    public class Volume24Hr
    {
    }

    public class VolumeAllCurrencies
    {
    }

    public class CirculatingSupply
    {
    }

    public class MaxSupply
    {
    }

    public class StartDate : IntJsonEntity
    {
    }
}
