﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities.Responses
{
    public class StockSearchResult
    {
        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("exch")]
        public string Exchange { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("exchDisp")]
        public string ExchangeDisp { get; set; }

        [JsonPropertyName("typeDisp")]
        public string TypeDisp { get; set; }
    }
}