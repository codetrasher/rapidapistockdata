﻿namespace RapidAPIStockData.Entities.Responses
{
    public class AutoCompleteSearchResult
    {
        public ResultSet ResultSet { get; set; }
    }
}
