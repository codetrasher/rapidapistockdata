﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities.Responses
{
    public class QuoteSummaryResult
    {
        [JsonPropertyName("quoteSummary")]
        public QuoteSummary QuoteSummary { get; set; }
    }

    public class QuoteSummary
    {
        [JsonPropertyName("result")]
        public IEnumerable<dynamic> Result { get; set; }

        [JsonIgnore]
        public Dictionary<string, object> ParseResult { get; private set; } = new();
    }
}
