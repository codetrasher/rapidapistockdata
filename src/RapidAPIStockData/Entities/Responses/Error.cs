﻿using RapidAPIStockData.Interfaces;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities.Responses
{
    public class Error : IResult
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
