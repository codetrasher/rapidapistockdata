﻿using System.Collections.Generic;

namespace RapidAPIStockData.Entities.Responses
{
    public class ResultSet
    {
        public List<StockSearchResult> Result { get; set; }
    }
}