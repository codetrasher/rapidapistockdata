﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class DefaultKeyStatistics
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("priceHint")]
        public PriceHint PriceHint { get; set; }

        [JsonPropertyName("enterpriseValue")]
        public EnterpriseValue EnterpriseValue { get; set; }

        [JsonPropertyName("forwardPE")]
        public ForwardPE ForwardPE { get; set; }

        [JsonPropertyName("profitMargins")]
        public ProfitMargins ProfitMargins { get; set; }

        [JsonPropertyName("floatShares")]
        public FloatShares FloatShares { get; set; }

        [JsonPropertyName("sharesOutstanding")]
        public SharesOutstanding SharesOutstanding { get; set; }

        [JsonPropertyName("sharesShort")]
        public SharesShort SharesShort { get; set; }

        [JsonPropertyName("sharesShortPriorMonth")]
        public SharesShortPriorMonth SharesShortPriorMonth { get; set; }

        [JsonPropertyName("sharesShortPreviousMonthDate")]
        public SharesShortPreviousMonthDate SharesShortPreviousMonthDate { get; set; }

        [JsonPropertyName("dateShortInterest")]
        public DateShortInterest DateShortInterest { get; set; }

        [JsonPropertyName("sharesPercentSharesOut")]
        public SharesPercentSharesOut SharesPercentSharesOut { get; set; }

        [JsonPropertyName("heldPercentInsiders")]
        public HeldPercentInsiders HeldPercentInsiders { get; set; }

        [JsonPropertyName("heldPercentInstitutions")]
        public HeldPercentInstitutions HeldPercentInstitutions { get; set; }

        [JsonPropertyName("shortRatio")]
        public ShortRatio ShortRatio { get; set; }

        [JsonPropertyName("shortPercentOfFloat")]
        public ShortPercentOfFloat ShortPercentOfFloat { get; set; }

        [JsonPropertyName("beta")]
        public Beta Beta { get; set; }

        [JsonPropertyName("impliedSharesOutstanding")]
        public ImpliedSharesOutstanding ImpliedSharesOutstanding { get; set; }

        [JsonPropertyName("morningStarOverallRating")]
        public MorningStarOverallRating MorningStarOverallRating { get; set; }

        [JsonPropertyName("morningStarRiskRating")]
        public MorningStarRiskRating MorningStarRiskRating { get; set; }

        [JsonPropertyName("category")]
        public object Category { get; set; }

        [JsonPropertyName("bookValue")]
        public BookValue BookValue { get; set; }

        [JsonPropertyName("priceToBook")]
        public PriceToBook PriceToBook { get; set; }

        [JsonPropertyName("annualReportExpenseRatio")]
        public AnnualReportExpenseRatio AnnualReportExpenseRatio { get; set; }

        [JsonPropertyName("ytdReturn")]
        public YtdReturn YtdReturn { get; set; }

        [JsonPropertyName("beta3Year")]
        public Beta3Year Beta3Year { get; set; }

        [JsonPropertyName("totalAssets")]
        public TotalAssets TotalAssets { get; set; }

        [JsonPropertyName("yield")]
        public Yield Yield { get; set; }

        [JsonPropertyName("fundFamily")]
        public object FundFamily { get; set; }

        [JsonPropertyName("fundInceptionDate")]
        public FundInceptionDate FundInceptionDate { get; set; }

        [JsonPropertyName("legalType")]
        public object LegalType { get; set; }

        [JsonPropertyName("threeYearAverageReturn")]
        public ThreeYearAverageReturn ThreeYearAverageReturn { get; set; }

        [JsonPropertyName("fiveYearAverageReturn")]
        public FiveYearAverageReturn FiveYearAverageReturn { get; set; }

        [JsonPropertyName("priceToSalesTrailing12Months")]
        public PriceToSalesTrailing12Months PriceToSalesTrailing12Months { get; set; }

        [JsonPropertyName("lastFiscalYearEnd")]
        public LastFiscalYearEnd LastFiscalYearEnd { get; set; }

        [JsonPropertyName("nextFiscalYearEnd")]
        public NextFiscalYearEnd NextFiscalYearEnd { get; set; }

        [JsonPropertyName("mostRecentQuarter")]
        public MostRecentQuarter MostRecentQuarter { get; set; }

        [JsonPropertyName("earningsQuarterlyGrowth")]
        public EarningsQuarterlyGrowth EarningsQuarterlyGrowth { get; set; }

        [JsonPropertyName("revenueQuarterlyGrowth")]
        public RevenueQuarterlyGrowth RevenueQuarterlyGrowth { get; set; }

        [JsonPropertyName("netIncomeToCommon")]
        public NetIncomeToCommon NetIncomeToCommon { get; set; }

        [JsonPropertyName("trailingEps")]
        public TrailingEps TrailingEps { get; set; }

        [JsonPropertyName("forwardEps")]
        public ForwardEps ForwardEps { get; set; }

        [JsonPropertyName("pegRatio")]
        public PegRatio PegRatio { get; set; }

        [JsonPropertyName("lastSplitFactor")]
        public string LastSplitFactor { get; set; }

        [JsonPropertyName("lastSplitDate")]
        public LastSplitDate LastSplitDate { get; set; }

        [JsonPropertyName("enterpriseToRevenue")]
        public EnterpriseToRevenue EnterpriseToRevenue { get; set; }

        [JsonPropertyName("enterpriseToEbitda")]
        public EnterpriseToEbitda EnterpriseToEbitda { get; set; }

        [JsonPropertyName("52WeekChange")]
        public FiftyTwoWeekChange _52WeekChange { get; set; }

        [JsonPropertyName("SandP52WeekChange")]
        public SandP52WeekChange SandP52WeekChange { get; set; }

        [JsonPropertyName("lastDividendValue")]
        public LastDividendValue LastDividendValue { get; set; }

        [JsonPropertyName("lastDividendDate")]
        public LastDividendDate LastDividendDate { get; set; }

        [JsonPropertyName("lastCapGain")]
        public LastCapGain LastCapGain { get; set; }

        [JsonPropertyName("annualHoldingsTurnover")]
        public AnnualHoldingsTurnover AnnualHoldingsTurnover { get; set; }
    }

    public class EnterpriseValue : LongJsonEntity
    {
    }

    public class ProfitMargins : DoubleJsonEntity
    {
    }

    public class FloatShares : LongJsonEntity
    {
    }

    public class SharesOutstanding : LongJsonEntity
    {
    }

    public class SharesShort : IntJsonEntity
    {
    }

    public class SharesShortPriorMonth : IntJsonEntity
    {
    }

    public class SharesShortPreviousMonthDate : IntJsonEntity
    {
    }

    public class DateShortInterest : IntJsonEntity
    {
    }

    public class SharesPercentSharesOut : DoubleJsonEntity
    {
    }

    public class HeldPercentInsiders : DoubleJsonEntity
    {
    }

    public class HeldPercentInstitutions : DoubleJsonEntity
    {
    }

    public class ShortRatio : DoubleJsonEntity
    {
    }

    public class ShortPercentOfFloat : DoubleJsonEntity
    {
    }

    public class ImpliedSharesOutstanding
    {
    }

    public class MorningStarOverallRating
    {
    }

    public class MorningStarRiskRating
    {
    }

    public class BookValue : DoubleJsonEntity
    {
    }

    public class PriceToBook : DoubleJsonEntity
    {
    }

    public class AnnualReportExpenseRatio
    {
    }

    public class Beta3Year
    {
    }

    public class TotalAssets : LongJsonEntity
    {
    }

    public class FundInceptionDate
    {
    }

    public class ThreeYearAverageReturn
    {
    }

    public class FiveYearAverageReturn
    {
    }

    public class LastFiscalYearEnd : IntJsonEntity
    {
    }

    public class NextFiscalYearEnd : IntJsonEntity
    {
    }

    public class MostRecentQuarter : IntJsonEntity
    {
    }

    public class EarningsQuarterlyGrowth : DoubleJsonEntity
    {
    }

    public class RevenueQuarterlyGrowth
    {
    }

    public class NetIncomeToCommon : LongJsonEntity
    {
    }

    public class TrailingEps : DoubleJsonEntity
    {
    }

    public class ForwardEps : DoubleJsonEntity
    {
    }

    public class LastSplitDate : IntJsonEntity
    {
    }

    public class EnterpriseToRevenue : DoubleJsonEntity
    {
    }

    public class EnterpriseToEbitda : DoubleJsonEntity
    {
    }

    public class FiftyTwoWeekChange : DoubleJsonEntity
    {
    }

    public class SandP52WeekChange : DoubleJsonEntity
    {
    }

    public class LastDividendValue : DoubleJsonEntity
    {
    }

    public class LastDividendDate : IntJsonEntity
    {
    }

    public class LastCapGain
    {
    }

    public class AnnualHoldingsTurnover
    {
    }
}
