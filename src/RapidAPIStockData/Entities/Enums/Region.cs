﻿using System.Reflection;

namespace RapidAPIStockData.Entities.Enums
{
    public enum Region
    {
        US,
        AU,
        CA,
        FR,
        DE,
        HK,
        IT,
        ES,
        GB,
        IN
    }

    internal static class RegionExtensions
    {
        public static string String(this Region region)
        {
            FieldInfo fi = region.GetType().GetField(region.ToString());
            return fi.Name ?? string.Empty;
        }
    }
}
