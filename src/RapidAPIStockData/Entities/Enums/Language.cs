﻿using System.Reflection;

namespace RapidAPIStockData.Entities.Enums
{
    public enum Language
    {
        EN,
        FR,
        DE,
        IT,
        ES,
        ZH
    }

    internal static class LanguageExtension
    {
        public static string String(this Language language)
        {
            FieldInfo fi = language.GetType().GetField(language.ToString());
            return fi.Name.ToLower() ?? string.Empty;
        }
    }
}
