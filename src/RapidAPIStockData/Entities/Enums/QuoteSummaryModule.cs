﻿using RapidAPIStockData.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RapidAPIStockData.Entities.Enums
{
    public enum QuoteSummaryModule
    {
        [Description("summaryDetail")]
        [MappedEntity(typeof(SummaryDetail))]
        SUMMARY_DETAIL,

        [Description("assetProfile")]
        [MappedEntity(typeof(AssetProfile))]
        ASSET_PROFILE,

        [Description("financialData")]
        [MappedEntity(typeof(FinancialData))]
        FINANCIAL_DATA,

        [Description("defaultKeyStatistics")]
        [MappedEntity(typeof(DefaultKeyStatistics))]
        DEFAULT_KEY_STATISTICS,

        [Description("calendarEvents")]
        [MappedEntity(typeof(CalendarEvents))]
        CALENDAR_EVENTS,

        [Description("incomeStatementHistory")]
        [MappedEntity(typeof(IncomeStatementHistory))]
        INCOME_STATEMENT_HISTORY,

        [Description("incomeStatementHistoryQuarterly")]
        [MappedEntity(typeof(IncomeStatementHistoryQuarterly))]
        INCOME_STATEMENT_HISTORY_QUART,

        [Description("cashflowStatementHistory")]
        [MappedEntity(typeof(CashflowStatementHistory))]
        CASHFLOW_STATEMENT_HISTORY,

        [Description("cashflowStatementHistoryQuarterly")]
        [MappedEntity(typeof(CashflowStatementHistoryQuarterly))]
        CASHFLOW_STATEMENT_HISTORY_QUART,

        [Description("balanceSheetHistory")]
        [MappedEntity(typeof(BalanceSheetHistory))]
        BALANCE_SHEET_HISTORY,

        [Description("balanceSheetHistoryQuarterly")]
        [MappedEntity(typeof(BalanceSheetHistoryQuarterly))]
        BALANCE_SHEET_HISTORY_QUART,

        [Description("earnings")]
        [MappedEntity(typeof(Earnings))]
        EARNINGS,

        [Description("earningsHistory")]
        [MappedEntity(typeof(EarningsHistory))]
        EARNING_HISTORY,

        [Description("earningsTrend")]
        [MappedEntity(typeof(EarningsTrend))]
        EARNINGS_TREND,

        [Description("insiderHolders")]
        [MappedEntity(typeof(InsiderHolders))]
        INSIDER_HOLDERS,

        [Description("insiderTransactions")]
        [MappedEntity(typeof(InsiderTransactions))]
        INSIDER_TRANSACTIONS,

        [Description("secFilings")]
        [MappedEntity(typeof(SecFilings))]
        SEC_FILINGS,

        [Description("indexTrend")]
        [MappedEntity(typeof(IndexTrend))]
        INDEX_TREND,

        [Description("netSharePurchaseActivity")]
        [MappedEntity(typeof(NetSharePurchaseActivity))]
        NET_SHARE_PURCHASE_ACTIVITY,

        [Description("upgradeDowngradeHistory")]
        [MappedEntity(typeof(UpgradeDowngradeHistory))]
        UPGRADE_DOWNGRADE_HISTORY,

        [Description("institutionOwnership")]
        [MappedEntity(typeof(InstitutionOwnership))]
        INSTITUTION_OWNERSHIP,

        [Description("recommendationTrend")]
        [MappedEntity(typeof(RecommendationTrend))]
        RECOMMENDATION_TREND,

        [Description("fundOwnership")]
        [MappedEntity(typeof(FundOwnership))]
        FUND_ONWERSHIP,

        [Description("majorDirectHolders")]
        [MappedEntity(typeof(MajorDirectHolders))]
        MAJOR_DIRECT_HOLDERS,

        [Description("majorHoldersBreakdown")]
        [MappedEntity(typeof(MajorHoldersBreakdown))]
        MAJOR_HOLDERS_BREAKDOWN,

        [Description("price")]
        [MappedEntity(typeof(Price))]
        PRICE,

        [Description("quoteType")]
        [MappedEntity(typeof(QuoteType))]
        QUOTE_TYPE,

        [Description("esgScores")]
        [MappedEntity(typeof(EsgScores))]
        ESG_SCORES
    }
    internal static class QuoteSummaryModuleExtensions
    {
        /// <summary>
        /// Get Enum's DescriptionAttribute as a string.
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static string GetDescription(this QuoteSummaryModule module)
        {
            DescriptionAttribute attribute = (DescriptionAttribute)module.GetType().GetField(module.ToString()).GetCustomAttribute(typeof(DescriptionAttribute));
            return attribute.Description;
        }

        public static Type GetMappedType(this QuoteSummaryModule module)
        {
            MappedEntityAttribute attribute = (MappedEntityAttribute)module.GetType().GetField(module.ToString()).GetCustomAttribute(typeof(MappedEntityAttribute));
            return attribute.MappedType;
        }

        /// <summary>
        /// Converts the list of QuoteSummaryModule enums to comma-separated list as a string.
        /// </summary>
        /// <param name="modules"></param>
        /// <returns></returns>
        public static string ToCommaSeparatedString(this IEnumerable<QuoteSummaryModule> modules)
        {
            var list = modules.ToList();
            if (list.Count == 0)
            {
                return "";
            }

            var builder = new StringBuilder();
            for (var i = 0; i < list.Count - 1; i++)
            {
                builder.Append($"{list[i].GetDescription()},");
            }
            builder.Append(list.LastOrDefault().GetDescription());

            return builder.ToString();
        }
    }

    public static class QuoteSummaryModuleHelper
    {
        public static Dictionary<string, QuoteSummaryModule> GetDictionary()
        {
            Dictionary<string, QuoteSummaryModule> dictionary = new();
            foreach (QuoteSummaryModule qsm in Enum.GetValues(typeof(QuoteSummaryModule)))
            {
                dictionary[qsm.GetDescription()] = qsm;
            }
            return dictionary;
        }

        public static IEnumerable<QuoteSummaryModule> GetAll()
        {
            var list = new List<QuoteSummaryModule>();
            foreach (QuoteSummaryModule module in Enum.GetValues(typeof(QuoteSummaryModule)))
            {
                list.Add(module);
            }
            return list;
        }
    }
}