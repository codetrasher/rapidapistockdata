﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public abstract class BaseJsonEntity
    {
        [JsonPropertyName("fmt")]
        public string Format { get; set; }

        [JsonPropertyName("longFmt")]
        public string LongFormat { get; set; }
    }

    public class IntJsonEntity : BaseJsonEntity
    {
        [JsonPropertyName("raw")]
        public int Raw { get; set; }
    }

    public class FloatJsonEntity : BaseJsonEntity
    {
        [JsonPropertyName("raw")]
        public float Raw { get; set; }
    }

    public class DoubleJsonEntity : BaseJsonEntity
    {
        [JsonPropertyName("raw")]
        public double Raw { get; set; }
    }

    public class LongJsonEntity : BaseJsonEntity
    {
        [JsonPropertyName("raw")]
        public long Raw { get; set; }
    }
}
