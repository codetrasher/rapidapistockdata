﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class Trend
    {
        [JsonPropertyName("period")]
        public string Period { get; set; }

        [JsonPropertyName("strongBuy")]
        public int StrongBuy { get; set; }

        [JsonPropertyName("buy")]
        public int Buy { get; set; }

        [JsonPropertyName("hold")]
        public int Hold { get; set; }

        [JsonPropertyName("sell")]
        public int Sell { get; set; }

        [JsonPropertyName("strongSell")]
        public int StrongSell { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("endDate")]
        public string EndDate { get; set; }

        [JsonPropertyName("growth")]
        public Growth Growth { get; set; }

        [JsonPropertyName("earningsEstimate")]
        public EarningsEstimate EarningsEstimate { get; set; }

        [JsonPropertyName("revenueEstimate")]
        public RevenueEstimate RevenueEstimate { get; set; }

        [JsonPropertyName("epsTrend")]
        public EpsTrend EpsTrend { get; set; }

        [JsonPropertyName("epsRevisions")]
        public EpsRevisions EpsRevisions { get; set; }
    }

    public class RecommendationTrend
    {
        [JsonPropertyName("trend")]
        public List<Trend> Trend { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class QuoteType
    {
        [JsonPropertyName("exchange")]
        public string Exchange { get; set; }

        [JsonPropertyName("quoteType")]
        public string QuoteTypeValue { get; set; }

        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("underlyingSymbol")]
        public string UnderlyingSymbol { get; set; }

        [JsonPropertyName("shortName")]
        public string ShortName { get; set; }

        [JsonPropertyName("longName")]
        public string LongName { get; set; }

        [JsonPropertyName("firstTradeDateEpochUtc")]
        public int FirstTradeDateEpochUtc { get; set; }

        [JsonPropertyName("timeZoneFullName")]
        public string TimeZoneFullName { get; set; }

        [JsonPropertyName("timeZoneShortName")]
        public string TimeZoneShortName { get; set; }

        [JsonPropertyName("uuid")]
        public string Uuid { get; set; }

        [JsonPropertyName("messageBoardId")]
        public string MessageBoardId { get; set; }

        [JsonPropertyName("gmtOffSetMilliseconds")]
        public int GmtOffSetMilliseconds { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }


    public class ReportDate : IntJsonEntity
    {
    }

    public class PctHeld : DoubleJsonEntity
    {
    }

    public class Position : IntJsonEntity
    {
    }

    public class Value : LongJsonEntity
    {
    }

    public class History
    {
        [JsonPropertyName("epochGradeDate")]
        public int EpochGradeDate { get; set; }

        [JsonPropertyName("firm")]
        public string Firm { get; set; }

        [JsonPropertyName("toGrade")]
        public string ToGrade { get; set; }

        [JsonPropertyName("fromGrade")]
        public string FromGrade { get; set; }

        [JsonPropertyName("action")]
        public string Action { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("epsActual")]
        public EpsActual EpsActual { get; set; }

        [JsonPropertyName("epsEstimate")]
        public EpsEstimate EpsEstimate { get; set; }

        [JsonPropertyName("epsDifference")]
        public EpsDifference EpsDifference { get; set; }

        [JsonPropertyName("surprisePercent")]
        public SurprisePercent SurprisePercent { get; set; }

        [JsonPropertyName("quarter")]
        public Quarter Quarter { get; set; }

        [JsonPropertyName("period")]
        public string Period { get; set; }
    }

    public class UpgradeDowngradeHistory
    {
        [JsonPropertyName("history")]
        public List<History> History { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class PreMarketChangePercent : DoubleJsonEntity
    {
    }

    public class PreMarketChange : DoubleJsonEntity
    {
    }

    public class PreMarketPrice : DoubleJsonEntity
    {
    }

    public class PostMarketChangePercent : DoubleJsonEntity
    {
    }

    public class PostMarketChange : DoubleJsonEntity
    {
    }

    public class PostMarketPrice : DoubleJsonEntity
    {
    }

    public class RegularMarketChangePercent : DoubleJsonEntity
    {
    }

    public class RegularMarketChange : DoubleJsonEntity
    {
    }

    public class RegularMarketPrice : DoubleJsonEntity
    {
    }

    public class AverageDailyVolume3Month : IntJsonEntity
    {
    }

    public class Price
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("preMarketChangePercent")]
        public PreMarketChangePercent PreMarketChangePercent { get; set; }

        [JsonPropertyName("preMarketChange")]
        public PreMarketChange PreMarketChange { get; set; }

        [JsonPropertyName("preMarketTime")]
        public int PreMarketTime { get; set; }

        [JsonPropertyName("preMarketPrice")]
        public PreMarketPrice PreMarketPrice { get; set; }

        [JsonPropertyName("preMarketSource")]
        public string PreMarketSource { get; set; }

        [JsonPropertyName("postMarketChangePercent")]
        public PostMarketChangePercent PostMarketChangePercent { get; set; }

        [JsonPropertyName("postMarketChange")]
        public PostMarketChange PostMarketChange { get; set; }

        [JsonPropertyName("postMarketTime")]
        public int PostMarketTime { get; set; }

        [JsonPropertyName("postMarketPrice")]
        public PostMarketPrice PostMarketPrice { get; set; }

        [JsonPropertyName("postMarketSource")]
        public string PostMarketSource { get; set; }

        [JsonPropertyName("regularMarketChangePercent")]
        public RegularMarketChangePercent RegularMarketChangePercent { get; set; }

        [JsonPropertyName("regularMarketChange")]
        public RegularMarketChange RegularMarketChange { get; set; }

        [JsonPropertyName("regularMarketTime")]
        public int RegularMarketTime { get; set; }

        [JsonPropertyName("priceHint")]
        public PriceHint PriceHint { get; set; }

        [JsonPropertyName("regularMarketPrice")]
        public RegularMarketPrice RegularMarketPrice { get; set; }

        [JsonPropertyName("regularMarketDayHigh")]
        public RegularMarketDayHigh RegularMarketDayHigh { get; set; }

        [JsonPropertyName("regularMarketDayLow")]
        public RegularMarketDayLow RegularMarketDayLow { get; set; }

        [JsonPropertyName("regularMarketVolume")]
        public RegularMarketVolume RegularMarketVolume { get; set; }

        [JsonPropertyName("averageDailyVolume10Day")]
        public AverageDailyVolume10Day AverageDailyVolume10Day { get; set; }

        [JsonPropertyName("averageDailyVolume3Month")]
        public AverageDailyVolume3Month AverageDailyVolume3Month { get; set; }

        [JsonPropertyName("regularMarketPreviousClose")]
        public RegularMarketPreviousClose RegularMarketPreviousClose { get; set; }

        [JsonPropertyName("regularMarketSource")]
        public string RegularMarketSource { get; set; }

        [JsonPropertyName("regularMarketOpen")]
        public RegularMarketOpen RegularMarketOpen { get; set; }

        [JsonPropertyName("strikePrice")]
        public StrikePrice StrikePrice { get; set; }

        [JsonPropertyName("openInterest")]
        public OpenInterest OpenInterest { get; set; }

        [JsonPropertyName("exchange")]
        public string Exchange { get; set; }

        [JsonPropertyName("exchangeName")]
        public string ExchangeName { get; set; }

        [JsonPropertyName("exchangeDataDelayedBy")]
        public int ExchangeDataDelayedBy { get; set; }

        [JsonPropertyName("marketState")]
        public string MarketState { get; set; }

        [JsonPropertyName("quoteType")]
        public string QuoteType { get; set; }

        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("underlyingSymbol")]
        public object UnderlyingSymbol { get; set; }

        [JsonPropertyName("shortName")]
        public string ShortName { get; set; }

        [JsonPropertyName("longName")]
        public string LongName { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("quoteSourceName")]
        public string QuoteSourceName { get; set; }

        [JsonPropertyName("currencySymbol")]
        public string CurrencySymbol { get; set; }

        [JsonPropertyName("fromCurrency")]
        public object FromCurrency { get; set; }

        [JsonPropertyName("toCurrency")]
        public object ToCurrency { get; set; }

        [JsonPropertyName("lastMarket")]
        public object LastMarket { get; set; }

        [JsonPropertyName("volume24Hr")]
        public Volume24Hr Volume24Hr { get; set; }

        [JsonPropertyName("volumeAllCurrencies")]
        public VolumeAllCurrencies VolumeAllCurrencies { get; set; }

        [JsonPropertyName("circulatingSupply")]
        public CirculatingSupply CirculatingSupply { get; set; }

        [JsonPropertyName("marketCap")]
        public MarketCap MarketCap { get; set; }
    }



    public class Avg : DoubleJsonEntity
    {
    }

    public class Low : DoubleJsonEntity
    {
    }

    public class High : DoubleJsonEntity
    {
    }

    public class YearAgoEps : DoubleJsonEntity
    {
    }

    public class NumberOfAnalysts : IntJsonEntity
    {
    }

    public class YearAgoRevenue : LongJsonEntity
    {
        [JsonPropertyName("raw")]
        public new long? Raw { get; set; }
    }

    public class RevenueEstimate
    {
        [JsonPropertyName("avg")]
        public Avg Avg { get; set; }

        [JsonPropertyName("low")]
        public Low Low { get; set; }

        [JsonPropertyName("high")]
        public High High { get; set; }

        [JsonPropertyName("numberOfAnalysts")]
        public NumberOfAnalysts NumberOfAnalysts { get; set; }

        [JsonPropertyName("yearAgoRevenue")]
        public YearAgoRevenue YearAgoRevenue { get; set; }

        [JsonPropertyName("growth")]
        public Growth Growth { get; set; }
    }

    public class Current : DoubleJsonEntity
    {
    }

    public class _7daysAgo : DoubleJsonEntity
    {
    }

    public class _30daysAgo : DoubleJsonEntity
    {
    }

    public class _60daysAgo : DoubleJsonEntity
    {
    }

    public class _90daysAgo : DoubleJsonEntity
    {
    }

    public class EpsTrend
    {
        [JsonPropertyName("current")]
        public Current Current { get; set; }

        [JsonPropertyName("7daysAgo")]
        public _7daysAgo _7daysAgo { get; set; }

        [JsonPropertyName("30daysAgo")]
        public _30daysAgo _30daysAgo { get; set; }

        [JsonPropertyName("60daysAgo")]
        public _60daysAgo _60daysAgo { get; set; }

        [JsonPropertyName("90daysAgo")]
        public _90daysAgo _90daysAgo { get; set; }
    }

    public class UpLast7days : IntJsonEntity
    {
    }

    public class UpLast30days : IntJsonEntity
    {
    }

    public class DownLast30days : IntJsonEntity
    {
    }

    public class DownLast90days
    {
    }

    public class EpsRevisions
    {
        [JsonPropertyName("upLast7days")]
        public UpLast7days UpLast7days { get; set; }

        [JsonPropertyName("upLast30days")]
        public UpLast30days UpLast30days { get; set; }

        [JsonPropertyName("downLast30days")]
        public DownLast30days DownLast30days { get; set; }

        [JsonPropertyName("downLast90days")]
        public DownLast90days DownLast90days { get; set; }
    }

    public class EpsActual : DoubleJsonEntity
    {
    }

    public class EpsEstimate : DoubleJsonEntity
    {
    }

    public class EpsDifference : DoubleJsonEntity
    {
    }

    public class SurprisePercent : DoubleJsonEntity
    {
    }

    public class Quarter : IntJsonEntity
    {
    }

    public class EarningsHistory
    {
        [JsonPropertyName("history")]
        public List<History> History { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class Actual : DoubleJsonEntity
    {
    }

    public class Estimate2 : DoubleJsonEntity
    {
    }

    public class Quarterly
    {
        [JsonPropertyName("date")]
        public string Date { get; set; }

        [JsonPropertyName("actual")]
        public Actual Actual { get; set; }

        [JsonPropertyName("estimate")]
        public Estimate Estimate { get; set; }

        [JsonPropertyName("revenue")]
        public Revenue Revenue { get; set; }

        [JsonPropertyName("earnings")]
        public Earnings Earnings { get; set; }
    }

    public class CurrentQuarterEstimate : DoubleJsonEntity
    {
    }

    public class Revenue : LongJsonEntity
    {
    }

    public class Yearly : IntJsonEntity
    {
    }

    public class CurrentPrice : DoubleJsonEntity
    {
    }

    public class TargetHighPrice : DoubleJsonEntity
    {
    }

    public class TargetLowPrice : DoubleJsonEntity
    {
    }

    public class TargetMeanPrice : DoubleJsonEntity
    {
    }

    public class TargetMedianPrice : DoubleJsonEntity
    {
    }

    public class RecommendationMean : DoubleJsonEntity
    {
    }

    public class NumberOfAnalystOpinions : IntJsonEntity
    {
    }

    public class TotalCash : LongJsonEntity
    {
    }

    public class TotalCashPerShare : DoubleJsonEntity
    {
    }

    public class Ebitda : LongJsonEntity
    {
    }

    public class TotalDebt : LongJsonEntity
    {
    }

    public class QuickRatio : DoubleJsonEntity
    {
    }

    public class CurrentRatio : DoubleJsonEntity
    {
    }

    public class DebtToEquity : DoubleJsonEntity
    {
    }

    public class RevenuePerShare : DoubleJsonEntity
    {
    }

    public class ReturnOnAssets : DoubleJsonEntity
    {
    }

    public class ReturnOnEquity : DoubleJsonEntity
    {
    }

    public class GrossProfits : LongJsonEntity
    {
    }

    public class FreeCashflow : LongJsonEntity
    {
    }

    public class OperatingCashflow : LongJsonEntity
    {
    }

    public class EarningsGrowth : DoubleJsonEntity
    {
    }

    public class RevenueGrowth : DoubleJsonEntity
    {
    }

    public class GrossMargins : DoubleJsonEntity
    {
    }

    public class EbitdaMargins : DoubleJsonEntity
    {
    }

    public class OperatingMargins : DoubleJsonEntity
    {
    }

    public class FinancialData
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("currentPrice")]
        public CurrentPrice CurrentPrice { get; set; }

        [JsonPropertyName("targetHighPrice")]
        public TargetHighPrice TargetHighPrice { get; set; }

        [JsonPropertyName("targetLowPrice")]
        public TargetLowPrice TargetLowPrice { get; set; }

        [JsonPropertyName("targetMeanPrice")]
        public TargetMeanPrice TargetMeanPrice { get; set; }

        [JsonPropertyName("targetMedianPrice")]
        public TargetMedianPrice TargetMedianPrice { get; set; }

        [JsonPropertyName("recommendationMean")]
        public RecommendationMean RecommendationMean { get; set; }

        [JsonPropertyName("recommendationKey")]
        public string RecommendationKey { get; set; }

        [JsonPropertyName("numberOfAnalystOpinions")]
        public NumberOfAnalystOpinions NumberOfAnalystOpinions { get; set; }

        [JsonPropertyName("totalCash")]
        public TotalCash TotalCash { get; set; }

        [JsonPropertyName("totalCashPerShare")]
        public TotalCashPerShare TotalCashPerShare { get; set; }

        [JsonPropertyName("ebitda")]
        public Ebitda Ebitda { get; set; }

        [JsonPropertyName("totalDebt")]
        public TotalDebt TotalDebt { get; set; }

        [JsonPropertyName("quickRatio")]
        public QuickRatio QuickRatio { get; set; }

        [JsonPropertyName("currentRatio")]
        public CurrentRatio CurrentRatio { get; set; }

        [JsonPropertyName("totalRevenue")]
        public TotalRevenue TotalRevenue { get; set; }

        [JsonPropertyName("debtToEquity")]
        public DebtToEquity DebtToEquity { get; set; }

        [JsonPropertyName("revenuePerShare")]
        public RevenuePerShare RevenuePerShare { get; set; }

        [JsonPropertyName("returnOnAssets")]
        public ReturnOnAssets ReturnOnAssets { get; set; }

        [JsonPropertyName("returnOnEquity")]
        public ReturnOnEquity ReturnOnEquity { get; set; }

        [JsonPropertyName("grossProfits")]
        public GrossProfits GrossProfits { get; set; }

        [JsonPropertyName("freeCashflow")]
        public FreeCashflow FreeCashflow { get; set; }

        [JsonPropertyName("operatingCashflow")]
        public OperatingCashflow OperatingCashflow { get; set; }

        [JsonPropertyName("earningsGrowth")]
        public EarningsGrowth EarningsGrowth { get; set; }

        [JsonPropertyName("revenueGrowth")]
        public RevenueGrowth RevenueGrowth { get; set; }

        [JsonPropertyName("grossMargins")]
        public GrossMargins GrossMargins { get; set; }

        [JsonPropertyName("ebitdaMargins")]
        public EbitdaMargins EbitdaMargins { get; set; }

        [JsonPropertyName("operatingMargins")]
        public OperatingMargins OperatingMargins { get; set; }

        [JsonPropertyName("profitMargins")]
        public ProfitMargins ProfitMargins { get; set; }

        [JsonPropertyName("financialCurrency")]
        public string FinancialCurrency { get; set; }
    }
}
