﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class IndexTrend
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("peRatio")]
        public PeRatio PeRatio { get; set; }

        [JsonPropertyName("pegRatio")]
        public PegRatio PegRatio { get; set; }

        [JsonPropertyName("estimates")]
        public List<Estimate> Estimates { get; set; }
    }

    public class PeRatio : DoubleJsonEntity
    {
    }

    public class PegRatio : DoubleJsonEntity
    {
    }

    public class Growth : DoubleJsonEntity
    {
    }

    public class Estimate
    {
        [JsonPropertyName("period")]
        public string Period { get; set; }

        [JsonPropertyName("growth")]
        public Growth Growth { get; set; }
    }
}
