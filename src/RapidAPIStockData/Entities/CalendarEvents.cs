﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class CalendarEvents
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("earnings")]
        public Earnings Earnings { get; set; }

        [JsonPropertyName("exDividendDate")]
        public ExDividendDate ExDividendDate { get; set; }

        [JsonPropertyName("dividendDate")]
        public DividendDate DividendDate { get; set; }
    }

    public class DividendDate : IntJsonEntity
    {
    }
}
