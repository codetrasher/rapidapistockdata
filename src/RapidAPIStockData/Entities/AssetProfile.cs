﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class TotalPay : IntJsonEntity
    {
    }

    public class ExercisedValue : IntJsonEntity
    {
    }

    public class UnexercisedValue : IntJsonEntity
    {
    }

    public class CompanyOfficer
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("age")]
        public int Age { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("yearBorn")]
        public int YearBorn { get; set; }

        [JsonPropertyName("fiscalYear")]
        public int FiscalYear { get; set; }

        [JsonPropertyName("totalPay")]
        public TotalPay TotalPay { get; set; }

        [JsonPropertyName("exercisedValue")]
        public ExercisedValue ExercisedValue { get; set; }

        [JsonPropertyName("unexercisedValue")]
        public UnexercisedValue UnexercisedValue { get; set; }
    }

    public class AssetProfile
    {
        [JsonPropertyName("address1")]
        public string Address1 { get; set; }

        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }

        [JsonPropertyName("zip")]
        public string Zip { get; set; }

        [JsonPropertyName("country")]
        public string Country { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }

        [JsonPropertyName("website")]
        public string Website { get; set; }

        [JsonPropertyName("industry")]
        public string Industry { get; set; }

        [JsonPropertyName("sector")]
        public string Sector { get; set; }

        [JsonPropertyName("longBusinessSummary")]
        public string LongBusinessSummary { get; set; }

        [JsonPropertyName("fullTimeEmployees")]
        public int FullTimeEmployees { get; set; }

        [JsonPropertyName("companyOfficers")]
        public List<CompanyOfficer> CompanyOfficers { get; set; }

        [JsonPropertyName("auditRisk")]
        public int AuditRisk { get; set; }

        [JsonPropertyName("boardRisk")]
        public int BoardRisk { get; set; }

        [JsonPropertyName("compensationRisk")]
        public int CompensationRisk { get; set; }

        [JsonPropertyName("shareHolderRightsRisk")]
        public int ShareHolderRightsRisk { get; set; }

        [JsonPropertyName("overallRisk")]
        public int OverallRisk { get; set; }

        [JsonPropertyName("governanceEpochDate")]
        public int GovernanceEpochDate { get; set; }

        [JsonPropertyName("compensationAsOfEpochDate")]
        public int CompensationAsOfEpochDate { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    /*public class Root
    {
        [JsonPropertyName("assetProfile")]
        public AssetProfile AssetProfile { get; set; }
    }*/


}
