﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class EsgScores
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("totalEsg")]
        public TotalEsg TotalEsg { get; set; }

        [JsonPropertyName("environmentScore")]
        public EnvironmentScore EnvironmentScore { get; set; }

        [JsonPropertyName("socialScore")]
        public SocialScore SocialScore { get; set; }

        [JsonPropertyName("governanceScore")]
        public GovernanceScore GovernanceScore { get; set; }

        [JsonPropertyName("ratingYear")]
        public int RatingYear { get; set; }

        [JsonPropertyName("ratingMonth")]
        public int RatingMonth { get; set; }

        [JsonPropertyName("highestControversy")]
        public double HighestControversy { get; set; }

        [JsonPropertyName("peerCount")]
        public int PeerCount { get; set; }

        [JsonPropertyName("esgPerformance")]
        public string EsgPerformance { get; set; }

        [JsonPropertyName("peerGroup")]
        public string PeerGroup { get; set; }

        [JsonPropertyName("relatedControversy")]
        public List<string> RelatedControversy { get; set; }

        [JsonPropertyName("peerEsgScorePerformance")]
        public PeerEsgScorePerformance PeerEsgScorePerformance { get; set; }

        [JsonPropertyName("peerGovernancePerformance")]
        public PeerGovernancePerformance PeerGovernancePerformance { get; set; }

        [JsonPropertyName("peerSocialPerformance")]
        public PeerSocialPerformance PeerSocialPerformance { get; set; }

        [JsonPropertyName("peerEnvironmentPerformance")]
        public PeerEnvironmentPerformance PeerEnvironmentPerformance { get; set; }

        [JsonPropertyName("peerHighestControversyPerformance")]
        public PeerHighestControversyPerformance PeerHighestControversyPerformance { get; set; }

        [JsonPropertyName("percentile")]
        public Percentile Percentile { get; set; }

        [JsonPropertyName("environmentPercentile")]
        public object EnvironmentPercentile { get; set; }

        [JsonPropertyName("socialPercentile")]
        public object SocialPercentile { get; set; }

        [JsonPropertyName("governancePercentile")]
        public object GovernancePercentile { get; set; }

        [JsonPropertyName("adult")]
        public bool Adult { get; set; }

        [JsonPropertyName("alcoholic")]
        public bool Alcoholic { get; set; }

        [JsonPropertyName("animalTesting")]
        public bool AnimalTesting { get; set; }

        [JsonPropertyName("catholic")]
        public bool Catholic { get; set; }

        [JsonPropertyName("controversialWeapons")]
        public bool ControversialWeapons { get; set; }

        [JsonPropertyName("smallArms")]
        public bool SmallArms { get; set; }

        [JsonPropertyName("furLeather")]
        public bool FurLeather { get; set; }

        [JsonPropertyName("gambling")]
        public bool Gambling { get; set; }

        [JsonPropertyName("gmo")]
        public bool Gmo { get; set; }

        [JsonPropertyName("militaryContract")]
        public bool MilitaryContract { get; set; }

        [JsonPropertyName("nuclear")]
        public bool Nuclear { get; set; }

        [JsonPropertyName("pesticides")]
        public bool Pesticides { get; set; }

        [JsonPropertyName("palmOil")]
        public bool PalmOil { get; set; }

        [JsonPropertyName("coal")]
        public bool Coal { get; set; }

        [JsonPropertyName("tobacco")]
        public bool Tobacco { get; set; }
    }

    public class TotalEsg : DoubleJsonEntity
    {
    }

    public class EnvironmentScore : DoubleJsonEntity
    {
    }

    public class SocialScore : DoubleJsonEntity
    {
    }

    public class GovernanceScore : DoubleJsonEntity
    {
    }

    public class PeerEsgScorePerformance
    {
        [JsonPropertyName("min")]
        public double Min { get; set; }

        [JsonPropertyName("avg")]
        public double Avg { get; set; }

        [JsonPropertyName("max")]
        public double Max { get; set; }
    }

    public class PeerGovernancePerformance
    {
        [JsonPropertyName("min")]
        public double Min { get; set; }

        [JsonPropertyName("avg")]
        public double Avg { get; set; }

        [JsonPropertyName("max")]
        public double Max { get; set; }
    }

    public class PeerSocialPerformance
    {
        [JsonPropertyName("min")]
        public double Min { get; set; }

        [JsonPropertyName("avg")]
        public double Avg { get; set; }

        [JsonPropertyName("max")]
        public double Max { get; set; }
    }

    public class PeerEnvironmentPerformance
    {
        [JsonPropertyName("min")]
        public double Min { get; set; }

        [JsonPropertyName("avg")]
        public double Avg { get; set; }

        [JsonPropertyName("max")]
        public double Max { get; set; }
    }

    public class PeerHighestControversyPerformance
    {
        [JsonPropertyName("min")]
        public double Min { get; set; }

        [JsonPropertyName("avg")]
        public double Avg { get; set; }

        [JsonPropertyName("max")]
        public double Max { get; set; }
    }

    public class Percentile : DoubleJsonEntity
    {
    }
}
