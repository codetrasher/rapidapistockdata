﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class MajorDirectHolders
    {
        [JsonPropertyName("holders")]
        public List<object> Holders { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class InsiderHolders
    {
        [JsonPropertyName("holders")]
        public List<Holder> Holders { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class Holder
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("relation")]
        public string Relation { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("transactionDescription")]
        public string TransactionDescription { get; set; }

        [JsonPropertyName("latestTransDate")]
        public LatestTransDate LatestTransDate { get; set; }

        [JsonPropertyName("positionDirect")]
        public PositionDirect PositionDirect { get; set; }

        [JsonPropertyName("positionDirectDate")]
        public PositionDirectDate PositionDirectDate { get; set; }

        [JsonPropertyName("positionIndirect")]
        public PositionIndirect PositionIndirect { get; set; }

        [JsonPropertyName("positionIndirectDate")]
        public PositionIndirectDate PositionIndirectDate { get; set; }
    }

    public class MajorHoldersBreakdown
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("insidersPercentHeld")]
        public InsidersPercentHeld InsidersPercentHeld { get; set; }

        [JsonPropertyName("institutionsPercentHeld")]
        public InstitutionsPercentHeld InstitutionsPercentHeld { get; set; }

        [JsonPropertyName("institutionsFloatPercentHeld")]
        public InstitutionsFloatPercentHeld InstitutionsFloatPercentHeld { get; set; }

        [JsonPropertyName("institutionsCount")]
        public InstitutionsCount InstitutionsCount { get; set; }
    }

    public class LatestTransDate : IntJsonEntity
    {
    }

    public class PositionDirect : IntJsonEntity
    {
    }

    public class PositionDirectDate : IntJsonEntity
    {
    }

    public class PositionIndirect : IntJsonEntity
    {
    }

    public class PositionIndirectDate : IntJsonEntity
    {
    }

    public class InsidersPercentHeld : DoubleJsonEntity
    {
    }

    public class InstitutionsPercentHeld : DoubleJsonEntity
    {
    }

    public class InstitutionsFloatPercentHeld : DoubleJsonEntity
    {
    }

    public class InstitutionsCount : IntJsonEntity
    {
    }

    public class InstitutionOwnership
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("ownershipList")]
        public List<OwnershipList> OwnershipList { get; set; }
    }

    public class OwnershipList
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("reportDate")]
        public ReportDate ReportDate { get; set; }

        [JsonPropertyName("organization")]
        public string Organization { get; set; }

        [JsonPropertyName("pctHeld")]
        public PctHeld PctHeld { get; set; }

        [JsonPropertyName("position")]
        public Position Position { get; set; }

        [JsonPropertyName("value")]
        public Value Value { get; set; }
    }

    public class FundOwnership
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("ownershipList")]
        public List<OwnershipList> OwnershipList { get; set; }
    }
}
