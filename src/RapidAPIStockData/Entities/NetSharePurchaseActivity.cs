﻿using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class NetSharePurchaseActivity
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("period")]
        public string Period { get; set; }

        [JsonPropertyName("buyInfoCount")]
        public BuyInfoCount BuyInfoCount { get; set; }

        [JsonPropertyName("buyInfoShares")]
        public BuyInfoShares BuyInfoShares { get; set; }

        [JsonPropertyName("buyPercentInsiderShares")]
        public BuyPercentInsiderShares BuyPercentInsiderShares { get; set; }

        [JsonPropertyName("sellInfoCount")]
        public SellInfoCount SellInfoCount { get; set; }

        [JsonPropertyName("sellInfoShares")]
        public SellInfoShares SellInfoShares { get; set; }

        [JsonPropertyName("sellPercentInsiderShares")]
        public SellPercentInsiderShares SellPercentInsiderShares { get; set; }

        [JsonPropertyName("netInfoCount")]
        public NetInfoCount NetInfoCount { get; set; }

        [JsonPropertyName("netInfoShares")]
        public NetInfoShares NetInfoShares { get; set; }

        [JsonPropertyName("netPercentInsiderShares")]
        public NetPercentInsiderShares NetPercentInsiderShares { get; set; }

        [JsonPropertyName("totalInsiderShares")]
        public TotalInsiderShares TotalInsiderShares { get; set; }
    }

    public class BuyInfoCount : IntJsonEntity
    {
    }

    public class BuyInfoShares : IntJsonEntity
    {
    }

    public class BuyPercentInsiderShares : DoubleJsonEntity
    {
    }

    public class SellInfoCount : IntJsonEntity
    {
    }

    public class SellInfoShares : IntJsonEntity
    {
    }

    public class SellPercentInsiderShares : DoubleJsonEntity
    {
    }

    public class NetInfoCount : IntJsonEntity
    {
    }

    public class NetInfoShares : IntJsonEntity
    {
    }

    public class NetPercentInsiderShares : DoubleJsonEntity
    {
    }

    public class TotalInsiderShares : IntJsonEntity
    {
    }
}
