﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{
    public class Earnings
    {
        [JsonPropertyName("earningsDate")]
        public List<EarningsDate> EarningsDate { get; set; }

        [JsonPropertyName("earningsAverage")]
        public EarningsAverage EarningsAverage { get; set; }

        [JsonPropertyName("earningsLow")]
        public EarningsLow EarningsLow { get; set; }

        [JsonPropertyName("earningsHigh")]
        public EarningsHigh EarningsHigh { get; set; }

        [JsonPropertyName("revenueAverage")]
        public RevenueAverage RevenueAverage { get; set; }

        [JsonPropertyName("revenueLow")]
        public RevenueLow RevenueLow { get; set; }

        [JsonPropertyName("revenueHigh")]
        public RevenueHigh RevenueHigh { get; set; }

        [JsonPropertyName("raw")]
        public object Raw { get; set; }

        [JsonPropertyName("fmt")]
        public string Fmt { get; set; }

        [JsonPropertyName("longFmt")]
        public string LongFmt { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("earningsChart")]
        public EarningsChart EarningsChart { get; set; }

        [JsonPropertyName("financialsChart")]
        public FinancialsChart FinancialsChart { get; set; }

        [JsonPropertyName("financialCurrency")]
        public string FinancialCurrency { get; set; }
    }

    public class EarningsAverage : DoubleJsonEntity
    {
    }

    public class EarningsLow : DoubleJsonEntity
    {
    }

    public class EarningsHigh : DoubleJsonEntity
    {
    }

    public class RevenueAverage : LongJsonEntity
    {
    }

    public class RevenueLow : LongJsonEntity
    {
    }

    public class RevenueHigh : LongJsonEntity
    {
    }

    public class EarningsDate : IntJsonEntity
    {
    }

    public class RetainedEarnings : LongJsonEntity
    {
    }

    public class EarningsEstimate
    {
        [JsonPropertyName("avg")]
        public Avg Avg { get; set; }

        [JsonPropertyName("low")]
        public Low Low { get; set; }

        [JsonPropertyName("high")]
        public High High { get; set; }

        [JsonPropertyName("yearAgoEps")]
        public YearAgoEps YearAgoEps { get; set; }

        [JsonPropertyName("numberOfAnalysts")]
        public NumberOfAnalysts NumberOfAnalysts { get; set; }

        [JsonPropertyName("growth")]
        public Growth Growth { get; set; }
    }

    public class EarningsChart
    {
        [JsonPropertyName("quarterly")]
        public List<Quarterly> Quarterly { get; set; }

        [JsonPropertyName("currentQuarterEstimate")]
        public CurrentQuarterEstimate CurrentQuarterEstimate { get; set; }

        [JsonPropertyName("currentQuarterEstimateDate")]
        public string CurrentQuarterEstimateDate { get; set; }

        [JsonPropertyName("currentQuarterEstimateYear")]
        public int CurrentQuarterEstimateYear { get; set; }

        [JsonPropertyName("earningsDate")]
        public List<EarningsDate> EarningsDate { get; set; }
    }

    public class FinancialsChart
    {
        [JsonPropertyName("yearly")]
        public List<Yearly> Yearly { get; set; }

        [JsonPropertyName("quarterly")]
        public List<Quarterly> Quarterly { get; set; }
    }

    public class EarningsTrend
    {
        [JsonPropertyName("trend")]
        public List<Trend> Trend { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }
}
