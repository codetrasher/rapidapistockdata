﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RapidAPIStockData.Entities
{

    public class InsiderTransactions
    {
        [JsonPropertyName("transactions")]
        public List<Transaction> Transactions { get; set; }

        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }
    }

    public class Transaction
    {
        [JsonPropertyName("maxAge")]
        public int MaxAge { get; set; }

        [JsonPropertyName("shares")]
        public Shares Shares { get; set; }

        [JsonPropertyName("value")]
        public Value Value { get; set; }

        [JsonPropertyName("filerUrl")]
        public string FilerUrl { get; set; }

        [JsonPropertyName("transactionText")]
        public string TransactionText { get; set; }

        [JsonPropertyName("filerName")]
        public string FilerName { get; set; }

        [JsonPropertyName("filerRelation")]
        public string FilerRelation { get; set; }

        [JsonPropertyName("moneyText")]
        public string MoneyText { get; set; }

        [JsonPropertyName("startDate")]
        public StartDate StartDate { get; set; }

        [JsonPropertyName("ownership")]
        public string Ownership { get; set; }
    }

    public class Shares : IntJsonEntity
    {
    }
}
