# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.2]
### Changed
- Forgot to update CHANGELOG.

## [0.4.1]
### Changed
- Change some object types to numeric types in JSON entities.

## [0.4.0]
### Added
- **StockService** with `GetStockDetails` endpoint.
- Unit tests.

### Changed
- Updated CHANGELOG.
- Added some documentation to README.

## [0.3.1]
### Fixed
- appveyor, GitVersion and tag-release configuration.

## [0.1.0]
### Added
- README, CHANGELOG, .gitignore, GitVersion.yml and appveyor.yml files.
- **MarketService** with `GetStockAutoComplete` endpoint to search matching stocks.
- Unit test to cover `GetStockAutoComplete` behaviour.

[0.4.2]: https://bitbucket.org/codetrasher/rapidapistockdata/src/0.4.2
[0.4.1]: https://bitbucket.org/codetrasher/rapidapistockdata/src/0.4.1
[0.4.0]: https://bitbucket.org/codetrasher/rapidapistockdata/src/0.4.0
[0.3.1]: https://bitbucket.org/codetrasher/rapidapistockdata/src/0.3.1
[0.1.0]: https://bitbucket.org/codetrasher/rapidapistockdata/src/0.1.0