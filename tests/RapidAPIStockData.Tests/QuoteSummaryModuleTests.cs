﻿using NUnit.Framework;
using RapidAPIStockData.Entities.Enums;
using System.Collections.Generic;

namespace RapidAPIStockData.Tests
{
    public class QuoteSummaryModulesTests
    {

        [TestCase(new QuoteSummaryModule[0], ExpectedResult = "")]
        [TestCase(new QuoteSummaryModule[1] { QuoteSummaryModule.SUMMARY_DETAIL }, ExpectedResult = "summaryDetail")]
        [TestCase(new QuoteSummaryModule[2] { QuoteSummaryModule.SUMMARY_DETAIL, QuoteSummaryModule.ASSET_PROFILE }, ExpectedResult = "summaryDetail,assetProfile")]
        [TestCase(new QuoteSummaryModule[3] { 
            QuoteSummaryModule.SUMMARY_DETAIL,
            QuoteSummaryModule.ASSET_PROFILE,
            QuoteSummaryModule.CALENDAR_EVENTS
        }, ExpectedResult = "summaryDetail,assetProfile,calendarEvents")]
        public string TestToCommaSeparatedList(IEnumerable<QuoteSummaryModule> modules)
        {
            return modules.ToCommaSeparatedString();
        }
    }
}