﻿using Microsoft.Extensions.Options;
using NUnit.Framework;
using RapidAPIStockData.Services;
using System.Collections.Generic;

namespace RapidAPIStockData.Tests
{
    [TestFixture]
    public abstract class BaseTest
    {
        protected IOptions<ClientOptions> _options;
        protected string _baseUrl;
        protected Dictionary<string, string> _headers;

        [OneTimeSetUp]
        public void Init()
        { 
            _options = Options.Create(new ClientOptions
            {
                ApiKey = "abracadabra",
                HostUrl = "testservice.com"
            });

            _baseUrl = "https://testservice.com";

            _headers = new Dictionary<string, string>
            {
                ["x-rapidapi-host"] = "testservice.com",
                ["x-rapidapi-key"] = _options.Value.ApiKey
            };
        }
    }
}
