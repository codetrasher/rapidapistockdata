﻿using NUnit.Framework;
using RapidAPIStockData.Entities;
using RapidAPIStockData.Entities.Enums;
using RapidAPIStockData.Entities.Responses;
using RapidAPIStockData.Interfaces;
using RapidAPIStockData.Services;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace RapidAPIStockData.Tests
{
    [TestFixture]
    public class StockServiceTests : BaseTest
    {
        const string TEST_ENDPOINT = "v11/finance/quoteSummary";

        IStockService _service;
        string _testUrl;

        [SetUp]
        public void Setup()
        {
            _testUrl = $"{_baseUrl}/{TEST_ENDPOINT}";
        }

        [Test]
        public void TestStockService_GetStockQuoteSummary_NoSymbol()
        {
            var modules = new List<QuoteSummaryModule>()
            {
                QuoteSummaryModule.SUMMARY_DETAIL
            };

            const string EMPTY_SYMBOL = " ";

            _service = new StockService(_options);
            var exception = Assert.Throws<ArgumentException>(() => _service.GetStockDetailsAsync(EMPTY_SYMBOL, modules), null);
            Assert.AreEqual("symbol can't be empty", exception.Message);
        }

        [Test]
        public async Task TestStockService_GetStockQuoteSummary_SingleModule()
        {
            var symbol = "aapl";
            var module = new List<QuoteSummaryModule> { QuoteSummaryModule.SUMMARY_DETAIL };
            var builder = new UriBuilder($"{_testUrl}/{symbol}")
            {
                Query = "modules="
            };
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["modules"] = module.ToCommaSeparatedString();
            builder.Query = query.ToString();

            var mockMessageHandler = new MockHttpMessageHandler();
            mockMessageHandler.When(builder.Uri.ToString()).Respond(() =>
            {
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StringContent(File.ReadAllText(TestContext.CurrentContext.TestDirectory + "/testdata/getstockdetailsresult.json"))
                };
                return Task.FromResult(response);
            });

            _service = new StockService(_options, new HttpClient(mockMessageHandler));
            var result = await _service.GetStockDetailsAsync(symbol, module);

            Assert.IsTrue(result.QuoteSummary.ParseResult != null);
            Assert.IsTrue(result.QuoteSummary.ParseResult["summaryDetail"] is SummaryDetail);
        }

        [Test]
        public async Task TestStockService_GetStockQuoteSummary_TwoModules()
        {
            var symbol = "aapl";
            
            var module = new List<QuoteSummaryModule> { QuoteSummaryModule.SUMMARY_DETAIL, QuoteSummaryModule.PRICE };
            var builder = new UriBuilder($"{_testUrl}/{symbol}")
            {
                Query = "modules="
            };
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["modules"] = module.ToCommaSeparatedString();
            query["lang"] = Language.FR.String();
            query["region"] = Region.FR.String();
            builder.Query = query.ToString();
            
            var mockMessageHandler = new MockHttpMessageHandler();
            mockMessageHandler.When(builder.Uri.ToString()).Respond(() =>
            {
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StringContent(File.ReadAllText(TestContext.CurrentContext.TestDirectory + "/testdata/getstockdetailsresult_two_modules_FR-fr.json"))
                };
                return Task.FromResult(response);
            });

            _service = new StockService(_options, new HttpClient(mockMessageHandler));
            var result = await _service.GetStockDetailsAsync(symbol, module, Language.FR, Region.FR);
            var parseResult = result.QuoteSummary.ParseResult;

            Assert.IsTrue(parseResult != null);
            Assert.IsTrue(parseResult["summaryDetail"] is SummaryDetail && parseResult["price"] is Price);

            var price = parseResult["price"] as Price;
            Assert.AreEqual("82 465 400,00", price.RegularMarketVolume.LongFormat);
        }

        [Test]
        public async Task TestStockService_GetStockQuoteSummary_AllModules()
        {
            var symbol = "aapl";

            var modules = QuoteSummaryModuleHelper.GetAll();
            var builder = new UriBuilder($"{_testUrl}/{symbol}")
            {
                Query = "modules="
            };
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["modules"] = modules.ToCommaSeparatedString();
            builder.Query = query.ToString();

            var mockMessageHandler = new MockHttpMessageHandler();
            mockMessageHandler.When(builder.Uri.ToString()).Respond(() =>
            {
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StringContent(File.ReadAllText(TestContext.CurrentContext.TestDirectory + "/testdata/getstockdetailsresult_all_modules.json"))
                };
                return Task.FromResult(response);
            });

            _service = new StockService(_options, new HttpClient(mockMessageHandler));
            var result = await _service.GetStockDetailsAsync(symbol, modules);
            var parseResult = result.QuoteSummary.ParseResult;

            Assert.IsTrue(parseResult.All(kvp => kvp.Value != null));
        }
    }
}
