using NUnit.Framework;
using RapidAPIStockData.Services.AutoCompleteService;
using System.Net.Http;
using RapidAPIStockData.Entities.Responses;
using System.IO;
using RapidAPIStockData.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using RapidAPIStockData.Entities.Enums;
using RichardSzalay.MockHttp;

namespace RapidAPIStockData.Tests
{
    [TestFixture]
    public class AutoCompleteServiceTests : BaseTest
    {
        IMarketService _service;
        readonly string testfileName = TestContext.CurrentContext.TestDirectory + "/testdata/autocompletesearchresult.json";

        [SetUp]
        public void Setup()
        {
            var autoCompleteSearchResultJson = File.ReadAllText(testfileName);
            var testUri = $"{_baseUrl}/v6/finance/autocomplete?query=appl&lang=en";

            var mockHandler = new MockHttpMessageHandler();
            mockHandler.When(testUri).Respond(() =>
            {
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StringContent(autoCompleteSearchResultJson)
                };
                return Task.FromResult(response);
            });

            _service = new MarketService(_options, new HttpClient(mockHandler));
        }

        [Test]
        public async Task TestAutoCompleteService()
        {
            var result = (List<StockSearchResult>)await _service.GetStockAutoComplete("appl", Language.EN);
            Assert.AreEqual(10, result.Count);
            Assert.AreEqual("Apple Inc.", result[0].Name);
        }

    }
}