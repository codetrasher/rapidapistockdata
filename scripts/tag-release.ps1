git config --global user.email "$($env:GIT_EMAIL)"
git config --global user.name "$($env:GIT_USERNAME)"
git config --global credential.helper store

Add-Content "$($env:USERPROFILE)\.git-credentials" "https://$($env:GIT_USERNAME):$($env:GIT_PASSWORD)@bitbucket.org`n"

git remote set-url origin https://$($env:GIT_USERNAME):$($env:GIT_PASSWORD)@bitbucket.org/$($env:APPVEYOR_REPO_NAME).git
#git remote add upstream https://$($env:GIT_USERNAME):$($env:GIT_PASSWORD)@bitbucket.org/$($env:APPVEYOR_REPO_NAME).git
git remote -v
git tag -a -m "v$($env:GitVersion_MajorMinorPatch)" -m "See CHANGELOG for more details about this release." $($env:GitVersion_MajorMinorPatch)
git push origin --tags --quiet